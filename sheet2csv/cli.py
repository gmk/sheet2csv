#!/usr/bin/env python3

import argparse
import pandas as pd

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="input sheet file (xlsx, xls, …)")
    parser.add_argument("-o", "--output", help="output CSV file")
    parser.add_argument("-q", "--quiet", action="store_true")
    args = parser.parse_args()

    input_file = args.input or "input.xlsx"
    output_file = args.output or "output.csv"

    with pd.ExcelFile(input_file) as xls_file:
        df = pd.read_excel(xls_file)

    df.to_csv(output_file, sep=";", header=False, index=False)
    if not args.quiet:
        print(df)

